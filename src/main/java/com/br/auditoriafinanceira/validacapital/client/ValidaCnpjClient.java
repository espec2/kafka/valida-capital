package com.br.auditoriafinanceira.validacapital.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ValidarCnpj", url = "https://www.receitaws.com.br/v1/cnpj/")
public interface ValidaCnpjClient {

    @GetMapping("{id}")
    ValidaCnpjDTO validaCnpjDTO (@PathVariable String id);
}
