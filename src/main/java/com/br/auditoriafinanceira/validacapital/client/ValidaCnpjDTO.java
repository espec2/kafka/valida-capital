package com.br.auditoriafinanceira.validacapital.client;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ValidaCnpjDTO {

    @JsonProperty("capital_social")
    public String capitalSocial;

    public ValidaCnpjDTO() {
    }

    public String getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(String capitalSocial) {
        this.capitalSocial = capitalSocial;
    }
}
