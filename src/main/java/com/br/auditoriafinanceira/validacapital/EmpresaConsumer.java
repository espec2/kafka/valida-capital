package com.br.auditoriafinanceira.validacapital;

import com.br.auditoriafinanceira.cadastrocnpj.model.Empresa;
import com.br.auditoriafinanceira.validacapital.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;

@Component
public class EmpresaConsumer {
    @Autowired
    private EmpresaService empresaService;

    @KafkaListener(topics = "spec2-andre-vinicius-2", groupId = "ValidarCnpj-1")
    public void receber(@Payload Empresa empresa){
        Empresa empresaValidada = empresaService.validarCnpj(empresa);
        empresaService.enviarAoKafka(empresaValidada);
        System.out.println(empresaValidada.getCnpj() + ", " + empresaValidada.getNome() + ", " + empresaValidada.getStatusCadastro());
    }
}
