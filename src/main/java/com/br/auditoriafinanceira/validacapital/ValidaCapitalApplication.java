package com.br.auditoriafinanceira.validacapital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ValidaCapitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidaCapitalApplication.class, args);
	}

}
