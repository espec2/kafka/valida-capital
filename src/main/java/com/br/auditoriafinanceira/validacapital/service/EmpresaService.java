package com.br.auditoriafinanceira.validacapital.service;

import com.br.auditoriafinanceira.cadastrocnpj.enums.StatusCadastro;
import com.br.auditoriafinanceira.cadastrocnpj.model.Empresa;
import com.br.auditoriafinanceira.validacapital.client.ValidaCnpjClient;
import com.br.auditoriafinanceira.validacapital.client.ValidaCnpjDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static java.lang.Long.parseLong;

@Service
public class EmpresaService {

    @Autowired
    private ValidaCnpjClient validaCnpjClient;

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public Empresa validarCnpj(Empresa empresa){
        ValidaCnpjDTO validaCnpjDTO = validaCnpjClient.validaCnpjDTO(empresa.getCnpj());

        if ( Double.parseDouble(validaCnpjDTO.capitalSocial) > 1000000){
            empresa.setStatusCadastro(StatusCadastro.Valido);
        }

        return empresa;
    }

    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec2-andre-vinicius-3", empresa);
    }
}
